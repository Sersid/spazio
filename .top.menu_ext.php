<?
$arResult = [];
if (\Bitrix\Main\Loader::includeModule('iblock')) {
    $rsSections = \Bitrix\Iblock\SectionTable::getList(
        [
            'filter' => [
                'IBLOCK_ID' => IBLOCK_ID_CATALOG,
                'ACTIVE' => 'Y',
                'DEPTH_LEVEL' => 1,
                '!ID' => IBLOCK_SECTION_ID_SAUCES,
            ],
            'select' => ['ID', 'NAME', 'CODE'],
        ]
    );
    while ($arItem = $rsSections->fetch()) {
        $url = empty($arItem['CODE']) ? 'section-' . $arItem['ID'] : $arItem['CODE'];
        $arResult[] = [$arItem['NAME'], '/#' . $url, [], [], ""];
    }
}
$aMenuLinks = array_merge($arResult, $aMenuLinks);
