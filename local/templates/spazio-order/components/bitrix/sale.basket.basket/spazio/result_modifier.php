<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
$arAllItems = $arResult['ITEMS'];
$arResult['ITEMS'] = [];
foreach ($arAllItems as $arItems) {
    foreach ($arItems as $arItem) {
        $arResult['ITEMS'][$arItem['PRODUCT_ID']] = $arItem;
    }
}
$arElements = \internal\Basket::getElements(array_keys($arResult['ITEMS']));
foreach ($arResult['ITEMS'] as $id => $arItem) {
    if (!isset($arElements[$id])) {
        continue;
    }
    $arResult['ITEMS'][$id] = array_merge($arResult['ITEMS'][$id], $arElements[$id]);
}
// Блоки "ОТЛИЧНО СОЧЕТАЕТСЯ С" и "РЕКОМЕНДУЕМ":
$arResult['COMBINED_WITH'] = [];
$arResult['RECOMMENDED'] = [];
$rsElements = CIBlockElement::GetList(
    ['SORT' => 'ASC'],
    [
        'IBLOCK_ID' => IBLOCK_ID_CATALOG,
        'ACTIVE' => 'Y',
        '!ID' => array_keys($arResult['ITEMS']),
        [
            'LOGIC' => 'OR',
            'IBLOCK_SECTION_ID' => IBLOCK_SECTION_ID_SAUCES,
            '!PROPERTY_IS_RECOMMENDED' => false,
        ]
    ],
    false,
    false,
    [
        'ID',
        'NAME',
        'IBLOCK_SECTION_ID',
        'PREVIEW_TEXT',
        'PREVIEW_PICTURE',
        'CATALOG_GROUP_1',
    ]
);
$arImagesId = [];
while ($arItem = $rsElements->Fetch()) {
    // ключ PRICE, для удобства в использовании
    $arItem['PRICE'] = floatval($arItem['CATALOG_PRICE_1']);
    $arItem['PRICE_FORMATTED'] = priceFormatted($arItem['PRICE']);
    // дополняем массив изображений
    if (!empty($arItem['PREVIEW_PICTURE'])) {
        $arImagesId[] = $arItem['PREVIEW_PICTURE'];
    }
    $key = $arItem['IBLOCK_SECTION_ID'] != IBLOCK_SECTION_ID_SAUCES ? 'RECOMMENDED' : 'COMBINED_WITH';
    $arResult[$key][$arItem['ID']] = $arItem;
}
// "заполняем" товары фотками
$arFiles = [];
if (!empty($arImagesId)) {
    $rsImages = \Bitrix\Main\FileTable::getList(['filter' => ['=ID' => $arImagesId]]);
    while ($arFile = $rsImages->fetch()) {
        $arFiles[$arFile['ID']] = CFile::GetFileSRC($arFile);
    }
}
foreach (['COMBINED_WITH', 'RECOMMENDED'] as $key) {
    foreach ($arResult[$key] as $id => $arElement) {
        $arResult[$key][$id]['IMAGE'] =
            (!empty($arElement['PREVIEW_PICTURE']) && !empty($arFiles[$arElement['PREVIEW_PICTURE']]))
                ? $arFiles[$arElement['PREVIEW_PICTURE']] : null;
    }
}
