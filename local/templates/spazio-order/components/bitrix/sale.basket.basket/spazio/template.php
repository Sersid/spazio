<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<ul class="orderItems">
    <? foreach ($arResult['ITEMS'] as $arItem) { ?>
    <li class="orderItems__item">
        <article class="orderItem">
            <div class="orderItem__info">
                <h2 class="orderItem__name"><?= $arItem['NAME'] ?></h2>
                <? if (!empty($arItem['PIZZA_SIZE'])) { ?>
                    <p class="orderItem__descr"><?= $arItem['PIZZA_SIZE'] ?></p>
                <? } elseif (!empty($arItem['WEIGHT'])) { ?>
                    <p class="orderItem__descr"><?= $arItem['WEIGHT'] ?></p>
                <? } ?>
                <? if (!empty($arItem['VOLUME'])) { ?>
                    <p class="orderItem__descr"><?= $arItem['VOLUME'] ?></p>
                <? } ?>
            </div>
            <div class="counter">
                <button aria-label="уменьшить количество товара" class="counter__button counter__button_decr"></button>
                <input type="text" value="<?= $arItem['QUANTITY'] ?>" readonly="readonly" class="counter__value"/>
                <button aria-label="увеличить количество товара в корзине" class="counter__button counter__button_incr"></button>
            </div>
            <p class="orderItem__price">
                <? if (!empty($arItem['SUM_DISCOUNT_PRICE'])) { ?>
                    <span class="price-old"><?= priceFormatted($arItem['SUM_DISCOUNT_PRICE'])?> <span class="currency">₽</span></span>
                    <br>
                <? } ?>
                <?= priceFormatted($arItem['SUM_VALUE']) ?> <span class="currency">₽</span>
            </p>
            <button aria-label="отменить выбор" class="orderItem__remove"></button>
        </article>
    </li>
    <? } ?>
</ul>
<? if (!empty($arResult['RECOMMENDED'])) { ?>
<section class="recomendation">
    <h2 class="heading heading_subheading">Рекомендуем</h2>
    <div class="recomendation__layout">
        <div class="recomendation__carousel">
            <? foreach ($arResult['RECOMMENDED'] as $arItem) { ?>
            <div class="recomendation__slide">
                <article class="orderTile">
                    <img src="<?= $arItem['IMAGE'] ?>" alt="<?= $arItem['NAME']?>" class="orderTile__picture" title="<?= $arItem['NAME']?>"/>
                    <div class="orderTile__body">
                        <h3 class="orderTile__name"><?= $arItem['NAME']?></h3>
                        <p class="orderTile__price"><?= $arItem['PRICE']?> <span class="currency">₽</span></p>
                        <button class="button button_long">В корзину</button>
                    </div>
                </article>
            </div>
            <? } ?>
        </div>
    </div>
</section>
<? } ?>
<? if (!empty($arResult['COMBINED_WITH'])) { ?>
<section class="combination">
    <h2 class="heading heading_subheading">Отлично сочетается с:</h2>
    <ul class="combination__list">
        <? foreach ($arResult['COMBINED_WITH'] as $arItem) { ?>
        <li class="combination__item">
            <article class="orderTile">
                <img src="<?= $arItem['IMAGE'] ?>" alt="<?= $arItem['NAME']?>" class="orderTile__picture" title="<?= $arItem['NAME']?>"/>
                <div class="orderTile__body">
                    <h3 class="orderTile__name"><?= $arItem['NAME']?></h3>
                    <p class="orderTile__price"><?= $arItem['PRICE']?> <span class="currency">₽</span></p>
                    <button class="button button_long">В корзину</button>
                </div>
            </article>
        </li>
        <? } ?>
    </ul>
</section>
<? } ?>
<div class="promoCode">
    <form class="promoCode__form js-promoCodeForm" method="post">
        <input placeholder="Промокод" class="promoCode__input js-promoCodeInput" type="text"/>
        <button class="promoCode__button">Применить</button>
    </form>
</div>
<div class="orderTotal">
    <div class="orderTotal__sum">
        <p class="orderTotal__label">Сумма заказа:</p>
        <? if (!empty($arResult['DISCOUNT_PRICE_ALL'])) { ?>
            <p class="orderTotal__price price-old"><?= priceFormatted($arResult['DISCOUNT_PRICE_ALL']) ?> <span class="currency">₽</span></p>
        <? } ?>
        <p class="orderTotal__price"><?= priceFormatted($arResult['allSum']) ?> <span class="currency">₽</span></p>
    </div>
    <div class="orderTotal__nav">
        <a href="/" class="orderTotal__link orderTotal__link_back">Вернуться в меню</a>
        <a href="order2.html" class="orderTotal__link orderTotal__link_forward">Оформить заказ</a></div>
</div>
