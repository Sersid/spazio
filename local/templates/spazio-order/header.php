<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

$assetsPath = '/local/templates/spazio/assets';
// Добавляем css файлы
$arCss = [
    'https://cdnjs.cloudflare.com/ajax/libs/jquery.nanoscroller/0.8.7/css/nanoscroller.min.css',
    'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/<?= $assetsPath ?>/owl.carousel.min.css',
    'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css',
    $assetsPath . '/styles/app.min.css',
];
foreach ($arCss as $css) {
    $APPLICATION->SetTemplateCSS($css);
}
// Добавляем js файлы
$arJs = [
    'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/jquery.nanoscroller/0.8.7/javascripts/jquery.nanoscroller.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js',
    $assetsPath . '/scripts/jquery.paroller.min.js',
    $assetsPath . '/scripts/scripts.js',
];
foreach ($arJs as $js) {
    $APPLICATION->AddHeadScript($js);
}
$request = \Bitrix\Main\Context::getCurrent()->getRequest();
?>
<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link sizes="192x192" href="/favicon-192x192.png" rel="icon" type="image/png">
    <link sizes="160x160" href="/favicon-160x160.png" rel="icon" type="image/png">
    <link sizes="96x96" href="/favicon-96x96.png" rel="icon" type="image/png">
    <link sizes="16x16" href="/favicon-16x16.png" rel="icon" type="image/png">
    <link sizes="32x32" href="/favicon-32x32.png" rel="icon" type="image/png">
    <? $APPLICATION->ShowHead(); ?>
    <title><? $APPLICATION->ShowTitle() ?></title>
</head>

<body class="page">
<? $APPLICATION->ShowPanel() ?>
<header class="orderHeader">
    <div class="container">
        <div class="orderHeader__layout">
            <a href="/" class="logo header__logo"><img src="<?= $assetsPath ?>/images/logo_invert.png" alt="<?= Loc::getMessage(
                    'LOGO_ALT'
                ) ?>" width="185" height="50" class="logo__image" title=""/></a>
            <ol class="steps steps_cart">
                <li class="steps__item steps__item_active">Корзина</li>
                <li class="steps__item <?= in_array($request->get('step'), [2, 3]) ? 'steps__item_active' : '' ?>">
                    Оформление заказа
                </li>
                <li class="steps__item <?= $request->get('step') == 3 ? 'steps__item_active' : '' ?>">Заказ принят</li>
            </ol>
        </div>
    </div>
</header>
<main class="main" role="main">
    <div class="orderContent">
        <h1 class="heading"><? $APPLICATION->ShowTitle() ?></h1>
