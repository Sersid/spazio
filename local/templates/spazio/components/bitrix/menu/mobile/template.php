<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;
if (!empty($arResult)) {
    echo '<button aria-label="' . Loc::getMessage('OPEN') . '" class="header__menuToggler"></button>';
    echo '<nav class="mobileSubmenu"><ul class="mobileSubmenu__list">';
    foreach($arResult as $arItem) {
        echo '<li class="mobileSubmenu__item"><a href="' . $arItem["LINK"] . '" class="mobileSubmenu__link';
        if ($arItem["SELECTED"]) {
            echo ' active';
        }
        if (isset($arItem['PARAMS']['cssClass'])) {
            echo ' ' . $arItem['PARAMS']['cssClass'];
        }
        echo '">' . $arItem["TEXT"] . '</a></li>';
    }
    echo '</ul><div class="mobileSubmenu__close"><button aria-label="' . Loc::getMessage('CLOSE') . '"></button></div></nav>';
}
