<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (!empty($arResult)) {
    echo '<nav class="menu header__menu"><ul class="menu__list">';
    foreach($arResult as $i => $arItem) {
        echo '<li class="menu__item"><a href="' . $arItem["LINK"] . '" class="menu__link';
        if ($arItem["SELECTED"]) {
            echo ' active';
        }
        echo '">' . $arItem["TEXT"] . '</a></li>';
        if (count($arResult) - 1 > $i) {
            echo '<li aria-hidden="true" class="menu__item menu__item_separator"></li>';
        }
    }
    echo '</ul></nav>';
}
