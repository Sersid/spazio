<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

$assetsPath = $APPLICATION->GetTemplatePath('assets');
$includePath = $APPLICATION->GetTemplatePath('include');
// Добавляем css файлы
$arCss = [
    'https://cdnjs.cloudflare.com/ajax/libs/jquery.nanoscroller/0.8.7/css/nanoscroller.min.css',
    'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css',
    'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css',
    $assetsPath . '/styles/app.min.css',
];
foreach ($arCss as $css) {
    $APPLICATION->SetTemplateCSS($css);
}
// Добавляем js файлы
$arJs = [
    'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/jquery.nanoscroller/0.8.7/javascripts/jquery.nanoscroller.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js',
    $assetsPath . '/scripts/jquery.paroller.min.js',
    $assetsPath . '/scripts/scripts.js',
];
foreach ($arJs as $js) {
    $APPLICATION->AddHeadScript($js);
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link sizes="192x192" href="/favicon-192x192.png" rel="icon" type="image/png">
    <link sizes="160x160" href="/favicon-160x160.png" rel="icon" type="image/png">
    <link sizes="96x96" href="/favicon-96x96.png" rel="icon" type="image/png">
    <link sizes="16x16" href="/favicon-16x16.png" rel="icon" type="image/png">
    <link sizes="32x32" href="/favicon-32x32.png" rel="icon" type="image/png">
    <? $APPLICATION->ShowHead(); ?>
    <title><? $APPLICATION->ShowTitle() ?></title>
</head>
<body class="page">
<? $APPLICATION->ShowPanel() ?>
<? if ($APPLICATION->GetCurPage(true) == '/index.php') { ?>
    <img src="<?= $assetsPath ?>/images/meteor.png" alt="#" class="prlxImage prlxImage_meteor" title="" />
    <img src="<?= $assetsPath ?>/images/satellite.png" alt="#" class="prlxImage prlxImage_satellite" title="" />
    <img src="<?= $assetsPath ?>/images/astronautPizza.png" alt="#" class="prlxImage prlxImage_astronautPizza" title="" />
    <img src="<?= $assetsPath ?>/images/mars.png" alt="#" class="prlxImage prlxImage_mars" title="" />
    <img src="<?= $assetsPath ?>/images/rocket.png" alt="#" class="prlxImage prlxImage_rocket" title="" />
    <img src="<?= $assetsPath ?>/images/astronautCola.png" alt="#" class="prlxImage prlxImage_astronautCola" title="" />
    <img src="<?= $assetsPath ?>/images/meteor2.png" alt="#" class="prlxImage prlxImage_meteor2" title="" />
    <img src="<?= $assetsPath ?>/images/astronautCola2.png" alt="#" class="prlxImage prlxImage_astronautCola2" title="" />
<? } ?>
<div class="wrapper">
    <div class="layout"></div>
    <div class="inner">
        <header class="header">
            <div class="header__top">
                <div class="container">
                    <div class="header__layout">
                        <a href="/" class="logo header__logo"><img src="<?= $assetsPath ?>/images/logo-big.png" alt="<?= Loc::getMessage(
                                'LOGO_ALT'
                            ) ?>" width="185" height="50" class="logo__image" title="<?= Loc::getMessage(
                                'LOGO_ALT'
                            ) ?>"/></a>
                        <div class="header__slogan">
                            <p class="header__label"><?= Loc::getMessage('DELIVERY') ?></p>
                            <p class="header__text"><? $APPLICATION->IncludeFile(
                                    $includePath . '/city.php',
                                    [],
                                    ['MODE' => 'text']
                                ) ?></p>
                        </div>
                        <div class="header__address">
                            <p class="header__label"><?= Loc::getMessage('ADDRESS') ?></p>
                            <p class="header__text"><? $APPLICATION->IncludeFile(
                                    $includePath . '/address.php',
                                    [],
                                    ['MODE' => 'text']
                                ) ?></p>
                        </div>
                        <div class="header__phone">
                            <p class="header__label"><?= Loc::getMessage('PHONE') ?></p>
                            <p class="header__text"><? $APPLICATION->IncludeFile(
                                    $includePath . '/phone.php',
                                    [],
                                    ['MODE' => 'text']
                                ) ?></p>
                        </div>
                        <? $APPLICATION->IncludeComponent(
                            "a1:pizzabasket",
                            "card-icon",
                            array(
                                "COMPONENT_TEMPLATE" => "card-icon",
                                "ORDER_PAGE" => "/order/"
                            ),
                            false
                        ); ?>
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "mobile",
                            Array(
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "MENU_CACHE_GET_VARS" => array(),
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "A",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "ROOT_MENU_TYPE" => "left",
                                "USE_EXT" => "N",
                            ),
                            false
                        ); ?>
                    </div>
                </div>
            </div>
            <div class="header__bottom">
                <div class="container">
                    <div class="header__layout">
                        <a href="/" class="logo logo_small"><img src="<?= $assetsPath ?>/images/logo-small.png" alt="<?= Loc::getMessage(
                                'LOGO_ALT'
                            ) ?>" width="45" height="30" class="logo__image" title="<?= Loc::getMessage(
                                'LOGO_ALT'
                            ) ?>"/></a>
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "main",
                            Array(
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "MENU_CACHE_GET_VARS" => array(),
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "A",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "ROOT_MENU_TYPE" => "top",
                                "USE_EXT" => "Y",
                            ),
                            false
                        ); ?>
                        <div class="header__controls">
<!--                            <div class="halves header__halves">-->
<!--                                <a href="#halves" class="halves__button jsPopup">Собери свою пиццу</a>-->
<!--                            </div>-->
                            <? $APPLICATION->IncludeComponent(
                                "a1:pizzabasket",
                                "card-icon-detail",
                                array(
                                    "COMPONENT_TEMPLATE" => "card-icon-detail",
                                    "ORDER_PAGE" => "/order/"
                                ),
                                false
                            ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <main class="main" role="main">
            <? if ($APPLICATION->GetCurPage(true) == '/index.php') { ?>
            <? $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "slides",
                Array(
                    "ACTIVE_DATE_FORMAT" => "",    // Формат показа даты
                    "ADD_SECTIONS_CHAIN" => "N",    // Включать раздел в цепочку навигации
                    "AJAX_MODE" => "N",    // Включить режим AJAX
                    "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
                    "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                    "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                    "AJAX_OPTION_STYLE" => "N",    // Включить подгрузку стилей
                    "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
                    "CACHE_GROUPS" => "N",    // Учитывать права доступа
                    "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                    "CACHE_TYPE" => "A",    // Тип кеширования
                    "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
                    "DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                    "DISPLAY_BOTTOM_PAGER" => "N",    // Выводить под списком
                    "DISPLAY_DATE" => "N",    // Выводить дату элемента
                    "DISPLAY_NAME" => "N",    // Выводить название элемента
                    "DISPLAY_PICTURE" => "N",    // Выводить изображение для анонса
                    "DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
                    "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
                    "FIELD_CODE" => array(),
                    "FILTER_NAME" => "",    // Фильтр
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
                    "IBLOCK_ID" => "1",    // Код информационного блока
                    "IBLOCK_TYPE" => "info",    // Тип информационного блока (используется только для проверки)
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",    // Включать инфоблок в цепочку навигации
                    "INCLUDE_SUBSECTIONS" => "N",    // Показывать элементы подразделов раздела
                    "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
                    "NEWS_COUNT" => "100",    // Количество новостей на странице
                    "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                    "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
                    "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                    "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
                    "PAGER_TEMPLATE" => ".default",    // Шаблон постраничной навигации
                    "PAGER_TITLE" => "Новости",    // Название категорий
                    "PARENT_SECTION" => "",    // ID раздела
                    "PARENT_SECTION_CODE" => "",    // Код раздела
                    "PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
                    "PROPERTY_CODE" => array(),
                    "SET_BROWSER_TITLE" => "N",    // Устанавливать заголовок окна браузера
                    "SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
                    "SET_META_DESCRIPTION" => "N",    // Устанавливать описание страницы
                    "SET_META_KEYWORDS" => "N",    // Устанавливать ключевые слова страницы
                    "SET_STATUS_404" => "N",    // Устанавливать статус 404
                    "SET_TITLE" => "N",    // Устанавливать заголовок страницы
                    "SHOW_404" => "N",    // Показ специальной страницы
                    "SORT_BY1" => "SORT",    // Поле для первой сортировки новостей
                    "SORT_BY2" => "ACTIVE_FROM",    // Поле для второй сортировки новостей
                    "SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
                    "SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
                    "STRICT_SECTION_CHECK" => "N",    // Строгая проверка раздела для показа списка
                ),
                false
            ); ?>
            <? } ?>
            <div class="container">
