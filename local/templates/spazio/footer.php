<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
</div>
</main>
<footer class="footer">
    <div class="container">
        <div class="footer__row">
            <div class="footer__col">
                <a href="#" class="logo footer__logo"><img src="<?= $assetsPath ?>/images/logo-big.png" alt="Пиццерия spazio"
                                                           width="185" height="50" class="logo__image" title=""/></a>
            </div>
            <div class="footer__col">
                <ul class="footer__paymentSystems">
                    <li class="footer__paymentSystem">
                        <img src="<?= $assetsPath ?>/images/visa.png" alt="принимаем к оплате visa" title=""/>
                    </li>
                    <li class="footer__paymentSystem">
                        <img src="<?= $assetsPath ?>/images/mir.png" alt="принимаем к оплате Мир" title=""/>
                    </li>
                    <li class="footer__paymentSystem">
                        <img src="<?= $assetsPath ?>/images/master-card.png" alt="принимаем к оплате master-card"
                             title=""/></li>
                </ul>
            </div>
            <div class="footer__col footer__col_last">
                <div class="footer__social">
                    <a href="#" target="_blank" aria-label="мы в вконтакте" class="footer__vk"></a><a
                            href="#" target="_blank" aria-label="мы в инстаграм" class="footer__insta"></a><a href="#" target="_blank"
                                                                                                              aria-label="мы в ютюб" class="footer__youtube"></a>
                </div>
            </div>
        </div>
        <div class="footer__row">
            <div class="footer__col">
                <p class="footer__copyrights">&copy; <?= date('Y') ?> Spazio pizza. Сургут</p>
            </div>
            <div class="footer__col"><a href="#" target="_blank" class="footer__policy">Политика
                    конфиденциальности</a></div>
            <div class="footer__col footer__col_last">
                <div class="footer__ref">
                    <p>Разработка и<br>продвижение сайта:</p>
                    <a href="https://a1-reklama.ru/" target="_blank"><img src="<?= $assetsPath ?>/images/logo-a1.png" alt="A1 Интернет Эксперт"
                                                     title=""/></a>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
</div>
</body>
</html>
