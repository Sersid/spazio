'use strict';

document.addEventListener("DOMContentLoaded", function() {
	const layout = document.querySelector('.layout');
	if (layout) {
		document.addEventListener('mousemove', (e) => {

			layout.style.cssText += `transform: translate(${-(e.clientX / 45)}px, ${-(e.clientY / 45)}px)`;
		});
	}

	//	переменные
	const tileControls = document.querySelectorAll('.tile__controls');

	const header = document.querySelector('.header');
	const checkWindowWidth = () => {
		return window.innerWidth > 600;
	};

	if (header) {

		const headerBottom = document.querySelector('.header__bottom');
		const main = document.querySelector('.main');
		const logo = document.querySelector('.header__logo img');
		const cartIcon = document.querySelector('.header__cartIcon');



		const stickHeaderBottom = () => {
            header.classList.remove('header_fixed');
			if (window.scrollY > 91) {
				headerBottom.classList.add('header__bottom_fixed');
			} else {
				headerBottom.classList.remove('header__bottom_fixed');
			}
		};

		const stickAllHeader = () => {
            headerBottom.classList.remove('header__bottom_fixed');
			if (window.scrollY > 1) {
				header.classList.add('header_fixed');
				main.style.paddingTop = '126px';
				logo.src = '/local/templates/spazio/assets/images/logo_invert.png';
				cartIcon.style.backgroundImage = 'url(assets/images/cart_invert.png)';
			} else {
				header.classList.remove('header_fixed');
				main.style.paddingTop = '0';
				logo.src = '/local/templates/spazio/assets/images/logo-big.png';
				cartIcon.style.backgroundImage = 'url(assets/images/cart_white.png)';
			}
		};

		const windowOnScroll = () => {
			stick();
		};

		const stick = () => {
			if (checkWindowWidth()) {
				stickHeaderBottom();
			} else {
				stickAllHeader();
			}
		}

		stick();

		window.addEventListener('scroll', windowOnScroll);

		const windowOnResize = () => {
			stick();
		};

		window.addEventListener('resize', windowOnResize);
	}


	$('.jsPopup').magnificPopup({
		type: 'inline'
	});


	$('.hero').owlCarousel({
		items: 1,
		autoplay:true,
		autoplayTimeout:4000,
		autoplayHoverPause:false,
		loop: true
	});

	$(".nano").nanoScroller();

	$('.select__toggler').on('click', function () {
		$(this).toggleClass('select__toggler_active');
		$(this).next().toggleClass('select__wrapper_visible');
	});

	$('.recomendation__carousel').owlCarousel({
		margin: 20,
		nav: true,
		dots: false,
		items: 1,
		responsive: {
			768: {
				items: 2
			}
		}

	});

	const createTooltip = (message) => {
		const tooltip = document.createElement('div');
		tooltip.classList.add('tooltip');
		tooltip.innerHTML = message;
		return tooltip;
	};

	const destroyTooltip = (el) => {
		setTimeout(()=> {
			el.querySelector('.tooltip').remove();
		}, 1000);
	};

	const showTooltip = (e,message) => {
		const self = e.trigger;
		self.appendChild(createTooltip(message));
		destroyTooltip(self);
	};

	const jsPromoCodeBtns = document.querySelectorAll('.jsPromoCode');
	const clipboard = new ClipboardJS(jsPromoCodeBtns);
	clipboard.on('success', function(e) {
		const message = 'Cкопирован';
		showTooltip(e, message);
	});
	clipboard.on('error', function(e) {
		console.log(e);
	});


	if (checkWindowWidth()) {
		$(".prlxImage_meteor").paroller({ factor: 0.5, factorXs: 0.2, type: 'foreground', direction: 'vertical' });
		$(".prlxImage_satellite").paroller({ factor: 0.2, factorXs: 0.3, type: 'foreground', direction: 'vertical' });
		$(".prlxImage_astronautPizza").paroller({ factor: 0.7, factorXs: 0.6, type: 'foreground', direction: 'vertical' });
		$(".prlxImage_mars").paroller({ factor: 0.5, factorXs: 0.2, type: 'foreground', direction: 'vertical' });
		$(".prlxImage_rocket").paroller({ factor: 0.2, factorXs: 0.3, type: 'foreground', direction: 'vertical' });
		$(".prlxImage_astronautCola").paroller({ factor: 0.7, factorXs: 0.6, type: 'foreground', direction: 'vertical' });
		$(".prlxImage_meteor2").paroller({ factor: 0.5, factorXs: 0.2, type: 'foreground', direction: 'vertical' });
		$(".prlxImage_astronautCola2").paroller({ factor: 0.2, factorXs: 0.3, type: 'foreground', direction: 'vertical' });
	}

	$('.header__menuToggler').on('click', function () {
		$(this).next().addClass('mobileSubmenu_open');
	});

	$('.mobileSubmenu__close button').on('click', function () {
		$(this).parent().parent().removeClass('mobileSubmenu_open');
	});
});
