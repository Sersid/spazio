<?php
define("IBLOCK_ID_CATALOG", 2); // ИД каталога
define("IBLOCK_SECTION_ID_SAUCES", 8); // ID раздела "Соусы"

// Регистрация класса для работы с корзиной
\Bitrix\Main\Loader::registerAutoLoadClasses(
    null,
    ['\internal\Basket' => '/local/php_interface/internal/Basket.php']
);

/**
 * Форматирование цены
 * @param $price
 *
 * @return string
 */
function priceFormatted($price)
{
    $price = floatval($price);
    $decimals = count(explode('.', $price)) == 2 ? 2 : 0;
    return number_format($price, $decimals, '.', ' ');
}

\Bitrix\Main\Loader::registerAutoLoadClasses(
    null,
    array(
        'PHPMailer\PHPMailer\PHPMailer' => '/local/php_interface/internal/PHPMailer/src/PHPMailer.php',
        'PHPMailer\PHPMailer\Exception' => '/local/php_interface/internal/PHPMailer/src/Exception.php',
        'PHPMailer\PHPMailer\SMTP' => '/local/php_interface/internal/PHPMailer/src/SMTP.php',
    )
);

use \PHPMailer\PHPMailer\PHPMailer;
AddEventHandler("main", "OnBeforeMailSend", "OnBeforeMailSendHandler");
function OnBeforeMailSendHandler($arMail)
{
    $arAddresses = PHPMailer::parseAddresses($arMail['TO']);
    if (empty($arAddresses)) {
        return false;
    }
    $mail = new PHPMailer();
    foreach ($arAddresses as $arAddress) {
        $mail->addAddress($arAddress['address'], $arAddress['name']);
    }
    $mail->setFrom('spaziopizza86@mail.ru');
    $mail->addReplyTo('spaziopizza86@mail.ru');
    $mail->CharSet = !empty($arMail['CHARSET']) ? $arMail['CHARSET'] : PHPMailer::CHARSET_UTF8;
    $mail->Subject = $arMail['SUBJECT'];
    $mail->Body = $arMail['BODY'];
    $mail->isHTML(isset($arMail['CONTENT_TYPE']) && $arMail['CONTENT_TYPE'] == 'html');
    if (!empty($arMail['ATTACHMENT'])) {
        foreach ($arMail['ATTACHMENT'] as $arAttachment) {
            $mail->addAttachment($arAttachment['PATH'], $arAttachment['NAME']);
        }
    }
    try {
        $mail->isSMTP();
        $mail->Host = 'smtp.mail.ru';
        $mail->SMTPAuth = true;
        $mail->Username = 'spaziopizza86@mail.ru';
        $mail->Password = 'R38ledyb';
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;
        $return = $mail->send();
        return $return;
    } catch (Exception $e) {
        $mail->isMail();
        $return = $mail->send();
        return $return;
    }
}
function custom_mail() {
    return true;
}
