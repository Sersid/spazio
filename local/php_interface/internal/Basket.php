<?php

namespace internal;

use Bitrix\Main\Loader;
use Bitrix\Sale;

/**
 * Класс для работы с корзиной
 * Class Basket
 * @package internal
 */
class Basket
{
    /**
     * Basket constructor.
     * @throws \Bitrix\Main\LoaderException
     */
    public function __construct()
    {
        Loader::includeModule('iblock');
        Loader::includeModule('sale');
    }

    /**
     * Site ID
     * @return string
     */
    private function _getSite()
    {
        return \Bitrix\Main\Context::getCurrent()->getSite();
    }

    /**
     * Обьект корзины
     * @return Sale\BasketBase
     */
    private function _getBasket()
    {
        return Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), $this->_getSite());
    }

    /**
     * Добавляет/изменяет содержимое корзины
     * @param $productId
     * @param $quantity
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\ArgumentTypeException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\NotSupportedException
     * @throws \Bitrix\Main\ObjectNotFoundException
     */
    public function set($productId, $quantity): array
    {
        $productId = (int)$productId;
        $quantity = (float)$quantity;
        if (empty($productId)) {
            return $this->get();
        }
        $basket = $this->_getBasket();
        if ($item = $basket->getExistsItem('catalog', $productId)) {
            if ($quantity <= 0) {
                $item->delete();
            } else {
                $item->setField('QUANTITY', $quantity);
            }
        }
        else {
            if ($quantity > 0) {
                $item = $basket->createItem('catalog', $productId);
                $item->setFields(
                    [
                        'QUANTITY' => $quantity,
                        'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                        'LID' => $this->_getSite(),
                        'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
                    ]
                );
            }
        }
        $basket->save();
        return $this->get();
    }

    /**
     * Содержимое корзины
     * @return array
     * @throws \Bitrix\Main\ArgumentNullException
     */
    public function get(): array
    {
        static $arResult;
        if (is_null($arResult)) {
            $arResult = [];
            $arResult['ITEMS'] = [];
            $basket = $this->_getBasket();
            /** @var Sale\BasketItemBase $basketItem */
            foreach ($basket->getBasketItems() as $basketItem) {
                $arItem = [
                    'ID' => $basketItem->getId(),
                    'PRODUCT_ID' => $basketItem->getProductId(),
                    'PRICE' => floatval($basketItem->getPrice()) * $basketItem->getQuantity(),
                    'NAME' => $basketItem->getField('NAME'),
                    'QUANTITY' => $basketItem->getQuantity(),
                    // заполняется из свойтв элемента
                    'PIZZA_SIZE' => '',
                    'WEIGHT' => '',
                    'VOLUME' => '',
                ];
                $arItem['PRICE_FORMATTED'] = priceFormatted($arItem['PRICE']);
                $arResult['ITEMS'][$arItem['PRODUCT_ID']] = $arItem;
            }
            if (!empty($arResult['ITEMS'])) {
                $arElements = static::getElements(array_keys($arResult['ITEMS']));
                foreach ($arResult['ITEMS'] as $id => $arItem) {
                    if (!isset($arElements[$id])) {
                        continue;
                    }
                    $arResult['ITEMS'][$id] = array_merge($arResult['ITEMS'][$id], $arElements[$id]);
                }
            }
            ksort($arResult['ITEMS']);
            $arResult['COUNT'] = $basket->count();
            $arResult['PRICE'] = floatval($basket->getPrice());
            $arResult['PRICE_FORMATTED'] = priceFormatted($arResult['PRICE']);
        }
        return $arResult;
    }

    /**
     * Элементы
     * @param array $arIds
     *
     * @return array
     */
    public static function getElements(array $arIds): array
    {
        $arResult = [];
        $rsElements = \CIBlockElement::GetList(
            ['SORT' => 'ASC'],
            [
                'ID' => $arIds,
                'IBLOCK_ID' => IBLOCK_ID_CATALOG,
                'ACTIVE' => 'Y',
            ],
            false,
            false,
            [
                'ID',
                'NAME',
                'IBLOCK_SECTION_ID',
                'PROPERTY_PIZZA_SIZE',
                'PROPERTY_VOLUME',
                'PROPERTY_WEIGHT',
            ]
        );
        $arSections = [];
        while ($arItem = $rsElements->Fetch()) {
            $arSections[$arItem['IBLOCK_SECTION_ID']][] = $arItem['ID'];
            $arResult[$arItem['ID']] = [
                'NAME' => $arItem['NAME'],
                'PIZZA_SIZE' => $arItem['PROPERTY_PIZZA_SIZE_VALUE'],
                'WEIGHT' => $arItem['PROPERTY_WEIGHT_VALUE'],
                'VOLUME' => $arItem['PROPERTY_VOLUME_VALUE'],
            ];
        }
        if (!empty($arSections)) {
            $rsSections = \CIBlockSection::GetList(
                ['DEPTH_LEVEL' => 'ASC', 'SORT' => 'ASC'],
                [
                    'IBLOCK_ID' => IBLOCK_ID_CATALOG,
                    'ACTIVE' => 'Y',
                    'ID' => array_keys($arSections),
                ],
                false,
                ['ID', 'NAME', 'DEPTH_LEVEL']
            );
            while ($arItem = $rsSections->Fetch()) {
                if ($arItem['DEPTH_LEVEL'] == 2) {
                    foreach ($arSections[$arItem['ID']] as $elementId) {
                        $arResult[$elementId]['NAME'] = $arItem['NAME'];
                    }
                }
            }
        }
        return $arResult;
    }

    /**
     * Содержимое корзины
     * @return array
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getList()
    {
        $class = new self();
        return $class->get();
    }

    /**
     * Добавление товара в корзину
     * @param $productId
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\ArgumentTypeException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\NotSupportedException
     * @throws \Bitrix\Main\ObjectNotFoundException
     */
    public static function add($productId): array
    {
        $class = new self();
        return $class->set($productId, 1);
    }

    /**
     * Изменение кол-ва в корзине
     * @param $productId
     * @param $quantity
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\ArgumentTypeException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\NotSupportedException
     * @throws \Bitrix\Main\ObjectNotFoundException
     */
    public static function setQuantity($productId, $quantity): array
    {
        $class = new self();
        return $class->set($productId, $quantity);
    }

    /**
     * Удаление элемента из корзины
     * @param $productId
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\NotImplementedException
     * @throws \Bitrix\Main\ObjectNotFoundException
     */
    public static function remove($productId): array
    {
        $class = new self();
        $basket = $class->_getBasket();
        if ($item = $basket->getExistsItem('catalog', $productId)) {
            $item->delete();
            $basket->save();
        }
        return $class->get();
    }
}
