<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

Loc::loadLanguageFile(__FILE__);

/**
 * Class A1PizzaCatalogComponentParams
 */
class A1PizzaCatalogComponentParams
{
    /**
     * @var array
     */
    protected $arParameters = [];

    /**
     * @var array Текущие значения
     */
    protected $arCurrentValues;

    /**
     * @var string Тип инфоблока
     */
    protected $IBLOCK_TYPE;

    /**
     * @var string ID инфоблока
     */
    protected $IBLOCK_ID;

    /**
     * @var array
     */
    private $_arItem;

    /**
     * @return A1PizzaCatalogComponentParams
     */
    public static function getInstance()
    {
        return new self();
    }

    /**
     * Устанавливает текущие значения
     *
     * @param array $arCurrentValues
     *
     * @return $this
     */
    public function setCurrentValues($arCurrentValues)
    {

        $arCurrentValues = !is_array($arCurrentValues) ? [] : $arCurrentValues;
        $this->arCurrentValues = $arCurrentValues;
        foreach ($arCurrentValues as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
        return $this;
    }

    /**
     * @param $var
     *
     * @return bool
     */
    public function isEmpty($var)
    {
        return empty($var) || trim($var) == '-';
    }

    /**
     * Проверяет наличие обязательных модулей
     * @return $this
     * @throws Exception
     */
    public function checkRequiredModules()
    {
        $arRequiredModules = [
            'iblock',
            'catalog',
        ];
        foreach ($arRequiredModules as $module) {
            if (!CModule::IncludeModule($module)) {
                throw new Exception(Loc::getMessage('MODULE_NOT_INSTALLED', ['#MODULE#' => $module]));
            }
        }
        return $this;
    }

    /**
     * Типы инфоблоков
     * @return array
     */
    public function getIblockTypes()
    {
        static $arTypes; // сокращаем запросы в бд

        if ($arTypes === null) {
            $arTypes = CIBlockParameters::GetIBlockTypes(["-" => " "]);
        }

        return $arTypes;
    }

    /**
     * ID инфоблока
     * @return $this
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function setIblockId()
    {
        $arIBlockTypes = $this->getIblockTypes();
        $this->arParameters["IBLOCK_TYPE"] = [
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("IBLOCK_TYPE"),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "N",
            "VALUES" => $arIBlockTypes,
            "REFRESH" => "Y",
            "DEFAULT" => "catalog",
        ];
        if ($this->isEmpty($this->IBLOCK_TYPE)) {
            if (isset($arIBlockTypes[$this->arParameters['IBLOCK_TYPE']['DEFAULT']])) {
                $this->IBLOCK_TYPE = $this->arParameters['IBLOCK_TYPE']['DEFAULT'];
            }
        }
        if (!$this->isEmpty($this->IBLOCK_TYPE)) {
            $arIBlocks = [];
            $arParams = [
                'select' => ['ID', 'NAME'],
                'filter' => ['IBLOCK_TYPE_ID' => $this->IBLOCK_TYPE],
                'order' => ['SORT' => 'ASC'],
            ];
            $rsIblocks = \Bitrix\Iblock\IblockTable::getList($arParams);
            while ($arIBlock = $rsIblocks->fetch()) {
                $arIBlocks[$arIBlock['ID']] = $arIBlock['NAME'];
            }

            $this->arParameters["IBLOCK_ID"] = [
                "PARENT" => "DATA_SOURCE",
                "NAME" => GetMessage("IBLOCK_ID"),
                "TYPE" => "LIST",
                "ADDITIONAL_VALUES" => "N",
                "VALUES" => $arIBlocks,
                "REFRESH" => "Y",
            ];
        }
        return $this;
    }

    /**
     * Тип базовой цены
     * @return mixed
     * @throws Exception
     */
    public function setPriceTypes()
    {
        $arPriceTypes = [];
        $rsPriceType = \Bitrix\Catalog\GroupTable::getList(['select' => ['ID', 'NAME']]);
        if ($arPriceType = $rsPriceType->fetch()) {
            $arPriceTypes[$arPriceType['ID']] = $arPriceType['NAME'];
        }
        $this->arParameters["PRICE_TYPE"] = [
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("PRICE_TYPE"),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "N",
            "VALUES" => $arPriceTypes,
            "REFRESH" => "N",
        ];
        return $this;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getComponentParameters()
    {
        return [
            "PARAMETERS" => $this->checkRequiredModules()->setIblockId()->setPriceTypes()->arParameters,
        ];
    }
}

$arComponentParameters =
    A1PizzaCatalogComponentParams::getInstance()->setCurrentValues($arCurrentValues)->getComponentParameters();
