$(function() {
    // Переключатель размера пиццы
    $(document).on('change', '.js-pizzaSize', function() {
        var val = $(this).val(),
            catalogItem = $(this).closest('.js-catalogItem'),
            obItems = catalogItem.data('items'),
            image = catalogItem.find('.js-ItemPicture'),
            weight = catalogItem.find('.js-weight'),
            buttonBox = catalogItem.find('.js-addToCartBox'),
            counterBox = catalogItem.find('.js-counterBox'),
            price = catalogItem.find('.js-price');
        if (val in obItems) {
            var item = obItems[val];
            catalogItem.data('product-id', val);
            catalogItem.data('in-basket', $(this).data('in-basket'));
            if (catalogItem.data('in-basket') == '1') {
                buttonBox.addClass('js-hidden');
                counterBox.removeClass('js-hidden');
            } else {
                buttonBox.removeClass('js-hidden');
                counterBox.addClass('js-hidden');
            }
            if (price.length > 0) {
                price.html(item.PRICE_FORMATTED);
            }
            if (item.IMAGE && image.length > 0) {
                image.attr('src', item.IMAGE);
            }
            if (item.WEIGHT && weight.length > 0) {
                weight.html(item.WEIGHT);
            }
        }
    });
    // добавление к корзину
    $(document).on('click', '.js-addToCart', function() {
        var catalogItem = $(this).closest('.js-catalogItem'),
            productId = catalogItem.data('product-id'),
            counterBox = catalogItem.find('.js-counterBox'),
            buttonBox = catalogItem.find('.js-addToCartBox');
        counterBox.removeClass('js-hidden');
        buttonBox.addClass('js-hidden');
        catalogItem.data('in-basket', 1);
        $('.js-pizzaSize[value=' + productId + ']').data('in-basket', 1);
        $.ajax({
            url: '/ajax/basket/add.php',
            data: {productId: productId},
            dataType: 'json',
            success: function(basket) {
                renderBasket(basket);
            }
        });
    });
    // уменьшить/увеличить количество
    var timerId;
    $(document).on('click', '.js-counter', function() {
        var catalogItem = $(this).closest('.js-catalogItem'),
            productId = catalogItem.data('product-id'),
            counterBox = catalogItem.find('.js-counterBox'),
            buttonBox = catalogItem.find('.js-addToCartBox'),
            quantityInput = counterBox.find('.js-quantity'),
            quantity = parseFloat(quantityInput.val());
        switch ($(this).data('action')) {
            case 'LESS':
                quantity -= 1;
                break;
            case 'MORE':
                quantity += 1;
                break;
        }
        if (quantity <= 0) {
            quantityInput.val(1);
            counterBox.addClass('js-hidden');
            buttonBox.removeClass('js-hidden');
            catalogItem.data('in-basket', 0);
            $('.js-pizzaSize[value=' + productId + ']').data('in-basket', 0);
        } else {
            quantityInput.val(quantity);
            counterBox.removeClass('js-hidden');
            buttonBox.addClass('js-hidden');
            catalogItem.data('in-basket', 1);
            $('.js-pizzaSize[value=' + productId + ']').data('in-basket', 1);
        }
        clearTimeout(timerId);
        timerId = setTimeout(function() {
            $.ajax({
                url: '/ajax/basket/set-quantity.php',
                data: {productId: productId, quantity: quantity},
                dataType: 'json',
                success: function(basket) {
                    renderBasket(basket);
                }
            });
        }, 500);
        return false;
    });
    // удаление из корзины
    $(document).on('click', '.js-remove', function() {
        var item = $(this).closest('.js-catalogItem'),
            productId = item.data('product-id'),
            catalogItem = $('.js-catalogItem[data-product-id=' + productId + ']'),
            pizzaSize = $('.js-pizzaSize[value=' + productId + ']');
        pizzaSize.data('in-basket', 0);
        if (pizzaSize.length > 0) {
            catalogItem = pizzaSize.closest('.js-catalogItem');
        }
        var counterBox = catalogItem.find('.js-counterBox'),
            buttonBox = catalogItem.find('.js-addToCartBox'),
            quantityInput = counterBox.find('.js-quantity');
        catalogItem.data('in-basket', 0);
        counterBox.addClass('js-hidden');
        buttonBox.removeClass('js-hidden');
        quantityInput.val(1);
        $.ajax({
            url: '/ajax/basket/remove.php',
            data: {productId: productId},
            dataType: 'json',
            success: function(basket) {
                renderBasket(basket);
            }
        });
        return false;
    });
    // ренден виджета корзины
    function renderBasket(basket) {
        $('.js-totalPrice').html(basket.PRICE_FORMATTED);
        $('.js-cartCounter').html(basket.COUNT);
        if (basket.COUNT == 0) {
            $('.js-cartCounter').addClass('js-hidden');
        } else {
            $('.js-cartCounter').removeClass('js-hidden');
        }
        var cartList = '';
        $.each(basket.ITEMS, function(i, item) {
            cartList += '<li class="cartDropdown__item js-catalogItem" data-product-id="' + item.PRODUCT_ID + '">\n'
                + '<p class="cartDropdown__name">' + item.NAME + '</p>\n';
            if (item.PIZZA_SIZE) {
                cartList += '<p class="cartDropdown__chars">' + item.PIZZA_SIZE + '</p>';
            }
            if (item.VOLUME) {
                cartList += '<p class="cartDropdown__chars">' + item.VOLUME + '</p>';
            }
            cartList += '<div class="counter counter_small js-counterBox">\n'
                + '<button aria-label="уменьшить количество товара" class="counter__button counter__button_decr js-counter" data-action="LESS"></button>\n'
                + '<input type="text" value="' + item.QUANTITY + '" readonly="readonly" class="counter__value js-quantity"/>\n'
                + '<button aria-label="увеличить количество товара в корзине" class="counter__button counter__button_incr js-counter" data-action="MORE"></button>\n'
                + '</div>'
                + '<p class="cartDropdown__price">' + item.PRICE_FORMATTED + ' <span class="currency">₽</span></p>\n'
                + '<button aria-label="удалить элемент из корзины" class="cartDropdown__remove js-remove"></button>\n'
                + '</li>';
        });
        $('.js-cartList').html(cartList);
    }
});
