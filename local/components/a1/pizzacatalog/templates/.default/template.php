<?php
/**
 * Created by PhpStorm.
 * User: SersPC
 * Date: 04.02.2019
 * Time: 16:02
 */

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
if (!empty($arResult['ERROR'])) {
    echo $arResult['ERROR'];
} else {
    if (!empty($arResult['SECTIONS'])) {
        foreach ($arResult['SECTIONS'] as $arSection) {
            if (empty($arSection['ITEMS'])) {
                continue;
            } ?>
            <section id="<?= $arSection['HTML_ID'] ?>" class="js-catalog">
                <h2 class="sectionHeading"><?= $arSection['NAME'] ?></h2>
                <ul class="catalog<?= $arSection['UF_VIEW'] == 'SMALL' ? ' catalog_small' : '' ?>">
                    <? foreach ($arSection['ITEMS'] as $arItem) { ?>
                        <li class="catalog__item js-catalogItem" data-items='<?= json_encode(
                            $arItem['SUB_ITEMS']
                        ) ?>' data-product-id="<?= $arItem['ID'] ?>" data-in-basket="<?= (int)$arItem['IN_BASKET'] ?>">
                            <article class="tile tile_default">
                                <picture class="tile__picture">
                                    <img src="<?= $arItem['IMAGE'] ?>" alt="<?= $arItem['NAME'] ?>" class="js-ItemPicture"/>
                                </picture>
                                <h3 class="tile__productName"><?= $arItem['NAME'] ?></h3>
                                <div class="tile__info">
                                    <p class="tile__ingredients"><?= $arItem['PREVIEW_TEXT'] ?></p>
                                    <? if (!empty($arItem['PROPERTY_WEIGHT_VALUE'])) { ?>
                                        <p class="tile__chars">
                                            <?= Loc::getMessage('WEIGHT') ?>
                                            <span class="js-weight"><?= $arItem['PROPERTY_WEIGHT_VALUE'] ?></span>
                                        </p>
                                    <? } ?>
                                    <? if (!empty($arItem['PROPERTY_VOLUME_VALUE'])) { ?>
                                        <p class="tile__chars"><?= $arItem['PROPERTY_VOLUME_VALUE'] ?></p>
                                    <? } ?>
                                </div>
                                <? if (!empty($arItem['SUB_ITEMS'])) { ?>
                                    <div class="tile__controls">
                                        <p class="tile__controlsHeading"><?= Loc::getMessage('SELECT_PIZZA_SIZE') ?>
                                            :</p>
                                        <? foreach ($arItem['SUB_ITEMS'] as $i => $arSubItem) {
                                            $inputId = $arItem['HTML_ID'] . '_' . $i;
                                            ?>
                                            <input type="radio" name="<?= $arItem['HTML_ID'] ?>" id="<?= $inputId; ?>"<?
                                            if ($arItem['ID'] == $arSubItem['ID']) {
                                                echo ' checked="checked"';
                                            } ?> value="<?= $arSubItem['ID'] ?>" class="tile__sizeToggler visuallyHidden js-pizzaSize" data-in-basket="<?= (int)$arSubItem['IN_BASKET'] ?>"/>
                                            <label for="<?= $inputId ?>" class="tile__label"><?= $arSubItem['PIZZA_SIZE'] ?></label>
                                        <? } ?>
                                    </div>
                                <? } ?>
                                <div class="tile__footer js-addToCartBox<?= $arItem['IN_BASKET'] ? ' js-hidden'
                                    : '' ?>">
                                    <button class="button button_addToCart js-addToCart">
                                        <span class="button__price">
                                            <span class="button__priceNum js-price"><?= $arItem['PRICE'] ?></span>
                                            <span class="currency">₽</span>
                                        </span>
                                        <span class="button__text button__text_cart"><?= Loc::getMessage(
                                                'ADD_TO_BASKET'
                                            ) ?></span>
                                    </button>
                                </div>
                                <div class="counter js-counterBox<?= !$arItem['IN_BASKET'] ? ' js-hidden' : '' ?>">
                                    <button aria-label="<?= Loc::getMessage(
                                        'COUNTER_LESS'
                                    ) ?>" class="counter__button counter__button_decr js-counter" data-action="LESS"></button>
                                    <input type="text" value="<?= $arItem['QUANTITY'] ?>" readonly="readonly" class="counter__value js-quantity"/>
                                    <button aria-label="<?= Loc::getMessage(
                                        'COUNTER_MORE'
                                    ) ?>" class="counter__button counter__button_incr js-counter" data-action="MORE"></button>
                                </div>
                            </article>
                        </li>
                    <? } ?>
                </ul>
            </section>
            <?
        }
    } else {
        echo Loc::getMessage('NOT_FOUND');
    }
}
