<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\FileTable;

/**
 * Class A1PizzaCatalogComponent
 * Класс для отображения ассортимента Spazio Pizza
 */
class A1PizzaCatalogComponent extends \CBitrixComponent
{
    /** @var array Массив разделов */
    public $arSections;

    /** @var array Массив элементов */
    public $arElements;

    /**
     * Проверка необходимых модулей и настроек компонента
     * @throws \Bitrix\Main\LoaderException
     * @throws \Exception
     */
    protected function check()
    {
        $arRequiredModules = ['iblock', 'catalog'];
        foreach ($arRequiredModules as $moduleName) {
            if (!\Bitrix\Main\Loader::includeModule($moduleName)) {
                throw new \Exception(Loc::getMessage('MODULE_NOT_FOUND', ['#MODULE#' => $moduleName]));
            }
        }
        if (empty($this->arParams['IBLOCK_ID'])) {
            throw new \Exception(Loc::getMessage('IBLOCK_ID_NOT_FOUND'));
        }
        if (empty($this->arParams['CATALOG_PRICE_TYPE'])) {
            throw new \Exception(Loc::getMessage('PRICE_TYPE_NOT_FOUND'));
        }
    }

    /**
     * @param $arParams
     *
     * @return array
     * @throws \Bitrix\Main\LoaderException
     * @throws \Exception
     */
    public function onPrepareComponentParams($arParams)
    {
        $arParams['IBLOCK_ID'] =
            empty($arParams['IBLOCK_ID']) && defined('IBLOCK_ID_CATALOG') ? IBLOCK_ID_CATALOG : $arParams['IBLOCK_ID'];
        $arParams['CATALOG_PRICE_TYPE'] = (int)$arParams['CATALOG_PRICE_TYPE'];
        return $arParams;
    }

    /**
     * Пользовательские значения выпающих списков
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getSectionEnums(): array
    {
        $arResult = [];
        $rsUserFields = \Bitrix\Main\UserFieldTable::getList(
            [
                'select' => ['ID'],
                'filter' => [
                    'ENTITY_ID' => 'IBLOCK_' . $this->arParams['IBLOCK_ID'] . '_SECTION',
                ],
            ]
        );
        $arIds = [];
        while ($arItem = $rsUserFields->fetch()) {
            $arIds[] = $arItem['ID'];
        }
        if (!empty($arIds)) {
            $rsUserFieldEnums = CUserFieldEnum::GetList(
                array(),
                array(
                    '=USER_FIELD_ID' => $arIds,
                )
            );
            while ($arItem = $rsUserFieldEnums->Fetch()) {
                $arResult[$arItem['ID']] = $arItem;
            }
        }
        return $arResult;
    }

    /**
     * Список разделов
     */
    public function setSections()
    {
        $this->arSections = [];
        $arSectionEnums = $this->getSectionEnums();
        $rsSections = CIBlockSection::GetList(
            ['DEPTH_LEVEL' => 'ASC', 'SORT' => 'ASC'],
            [
                'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
                'ACTIVE' => 'Y',
                '!ID' => IBLOCK_SECTION_ID_SAUCES,
            ],
            false,
            ['ID', 'NAME', 'CODE', 'IBLOCK_SECTION_ID', 'UF_VIEW']
        );
        while ($arItem = $rsSections->Fetch()) {
            // id тега для навигации внутри страницы
            $arItem['HTML_ID'] = empty($arItem['CODE']) ? 'section-' . $arItem['ID'] : $arItem['CODE'];
            // XML_ID, для переключения отображения товаров (по 3 или по 4)
            if (isset($arSectionEnums[$arItem['UF_VIEW']])) {
                $arItem['UF_VIEW'] = $arSectionEnums[$arItem['UF_VIEW']]['XML_ID'];
            }
            $this->arSections[$arItem['ID']] = $arItem;
        }
    }

    /**
     * Список элементов
     * @throws Exception
     */
    public function setElements()
    {
        if (is_null($this->arSections)) {
            $this->setSections();
        }
        $this->arElements = [];
        if (empty($this->arSections)) {
            return $this->arElements;
        }
        $rsElements = CIBlockElement::GetList(
            ['SORT' => 'ASC'],
            [
                'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
                'ACTIVE' => 'Y',
                'IBLOCK_SECTION_ID' => array_keys($this->arSections),
            ],
            false,
            false,
            [
                'ID',
                'NAME',
                'IBLOCK_SECTION_ID',
                'PREVIEW_TEXT',
                'PREVIEW_PICTURE',
                'CATALOG_GROUP_' . $this->arParams['CATALOG_PRICE_TYPE'],
                'PROPERTY_PIZZA_SIZE',
                'PROPERTY_WEIGHT',
                'PROPERTY_VOLUME',
            ]
        );
        $arImagesId = [];
        $arBasket = \internal\Basket::getList();
        while ($arItem = $rsElements->Fetch()) {
            // ключ PRICE, для удобства в использовании
            $arItem['PRICE'] = floatval($arItem['CATALOG_PRICE_' . $this->arParams['CATALOG_PRICE_TYPE']]);
            $arItem['PRICE_FORMATTED'] = priceFormatted($arItem['PRICE']);
            // дополняем массив изображений
            if (!empty($arItem['PREVIEW_PICTURE'])) {
                $arImagesId[] = $arItem['PREVIEW_PICTURE'];
            }
            // Есть в корзине
            $arItem['IN_BASKET'] = isset($arBasket['ITEMS'][$arItem['ID']]);
            $arItem['QUANTITY'] = isset($arBasket['ITEMS'][$arItem['ID']]) ? $arBasket['ITEMS'][$arItem['ID']]['QUANTITY'] : 1;

            $this->arElements[$arItem['IBLOCK_SECTION_ID']][$arItem['ID']] = $arItem;
        }
        // "заполняем" товары фотками
        $arFiles = $this->getImages($arImagesId);
        foreach ($this->arElements as $sectionId => $arElements) {
            foreach ($arElements as $id => $arElement) {
                $this->arElements[$sectionId][$id]['IMAGE'] =
                    (!empty($arElement['PREVIEW_PICTURE']) && !empty($arFiles[$arElement['PREVIEW_PICTURE']]))
                        ? $arFiles[$arElement['PREVIEW_PICTURE']] : null;
            }
        }
    }

    /**
     * Массив изображений
     *
     * @param array $arFilesId
     *
     * @return array
     */
    public function getImages($arFilesId)
    {
        $arFiles = [];
        if (!empty($arFilesId)) {
            $rsImages = FileTable::getList(['filter' => ['=ID' => $arFilesId]]);
            while ($arFile = $rsImages->fetch()) {
                $arFiles[$arFile['ID']] = CFile::GetFileSRC($arFile);
            }
        }
        return $arFiles;
    }

    /**
     * Собираем воедино массив для вывода
     * @return array
     */
    public function getExecute(): array
    {
        $arResult = [];
        foreach ($this->arSections as $arCurrentSection) {
            if (empty($arCurrentSection['IBLOCK_SECTION_ID'])) {
                // первый уровень
                $arCurrentSection['ITEMS'] = array_values($this->arElements[$arCurrentSection['ID']]);
                $arResult[$arCurrentSection['ID']] = $arCurrentSection;
            } else {
                // группировать элементы по разделу, для выбора размеров пиццы
                $arSection = $this->arSections[$arCurrentSection['IBLOCK_SECTION_ID']];
                $arItem = [];
                foreach ($this->arElements[$arCurrentSection['ID']] as $arElement) {
                    if (empty($arItem)) {
                        $arItem = $arElement;
                        $arItem['NAME'] = $arCurrentSection['NAME'];
                        $arItem['HTML_ID'] = $arCurrentSection['HTML_ID'];
                    }
                    // общее изображение на всех
                    if (empty($arItem['IMAGE']) && !empty($arElement['IMAGE'])) {
                        $arItem['IMAGE'] = $arElement['IMAGE'];
                    }
                    // общее описание на всех
                    if (empty($arItem['PREVIEW_TEXT']) && !empty($arElement['PREVIEW_TEXT'])) {
                        $arItem['PREVIEW_TEXT'] = $arElement['PREVIEW_TEXT'];
                    }
                    $arItem['SUB_ITEMS'][$arElement['ID']] = [
                        'ID' => $arElement['ID'],
                        'PIZZA_SIZE' => $arElement['PROPERTY_PIZZA_SIZE_VALUE'],
                        'PRICE' => $arElement['PRICE'],
                        'PRICE_FORMATTED' => $arElement['PRICE_FORMATTED'],
                        'IMAGE' => $arElement['IMAGE'],
                        'WEIGHT' => $arElement['PROPERTY_WEIGHT_VALUE'],
                        'VOLUME' => $arElement['PROPERTY_VOLUME_VALUE'],
                        'IN_BASKET' => $arElement['IN_BASKET'],
                    ];
                }
                if (empty($arResult[$arSection['ID']])) {
                    $arResult[$arSection['ID']] = $arSection;
                }
                $arResult[$arSection['ID']]['ITEMS'][] = $arItem;
            }
        }
        return $arResult;
    }

    /**
     * @inheritdoc
     */
    public function executeComponent()
    {
        try {
            // Проверка необходимых модулей и настроек компонента
            $this->check();
            // Получаем массив разделов
            $this->setSections();
            // Получаем массив элементов
            $this->setElements();
            // Собираем все воедино
            $this->arResult['SECTIONS'] = $this->getExecute();
        } catch (Exception $e) {
            $this->arResult['ERROR'] = $e->getMessage();
        }
        $this->includeComponentTemplate();
    }
}
