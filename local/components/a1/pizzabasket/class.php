<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * Class A1PizzaBasketComponent
 * Класс для отображения корзины Spazio Pizza
 */
class A1PizzaBasketComponent extends \CBitrixComponent
{
    /**
     * @inheritdoc
     */
    public function onPrepareComponentParams($arParams)
    {
        $arParams['SHOW_RECOMMENDED'] =
        empty($arParams['SHOW_RECOMMENDED']) || $arParams['SHOW_RECOMMENDED'] == 'N' ? 'N' : 'Y';
        $arParams['SHOW_COMBINED_WITH'] =
            empty($arParams['SHOW_COMBINED_WITH']) || $arParams['SHOW_COMBINED_WITH'] == 'N' ? 'N' : 'Y';
        return $arParams;
    }

    /**
     * @inheritdoc
     */
    public function executeComponent()
    {
        $this->arResult = \internal\Basket::getList();
        // Блоки "ОТЛИЧНО СОЧЕТАЕТСЯ С" и "РЕКОМЕНДУЕМ":
        if (!empty($this->arResult['ITEMS'])
            && ($this->arParams['SHOW_RECOMMENDED'] == 'Y'
                || $this->arParams['SHOW_COMBINED_WITH'] == 'Y')) {
            $arKeys = [];
            $arFilter = [];
            if ($this->arParams['SHOW_RECOMMENDED'] == 'Y') {
                $arKeys[] = 'RECOMMENDED';
                $this->arResult['RECOMMENDED'] = [];
                $arFilter['!PROPERTY_IS_RECOMMENDED'] = false;
            }
            if ($this->arParams['SHOW_COMBINED_WITH'] == 'Y') {
                $arKeys[] = 'COMBINED_WITH';
                $this->arResult['COMBINED_WITH'] = [];
                $arFilter['IBLOCK_SECTION_ID'] = IBLOCK_SECTION_ID_SAUCES;
            }
            if (count($arFilter) > 1) {
                $arFilter['LOGIC'] = 'OR';
            }

            $arFilter = [
                'IBLOCK_ID' => IBLOCK_ID_CATALOG,
                'ACTIVE' => 'Y',
                '!ID' => array_keys($this->arResult['ITEMS']),
                $arFilter,
            ];

            $rsElements = CIBlockElement::GetList(
                ['SORT' => 'ASC'],
                $arFilter,
                false,
                false,
                [
                    'ID',
                    'NAME',
                    'IBLOCK_SECTION_ID',
                    'PREVIEW_TEXT',
                    'PREVIEW_PICTURE',
                    'CATALOG_GROUP_1',
                ]
            );
            $arImagesId = [];
            while ($arItem = $rsElements->Fetch()) {
                // ключ PRICE, для удобства в использовании
                $arItem['PRICE'] = floatval($arItem['CATALOG_PRICE_1']);
                $arItem['PRICE_FORMATTED'] = priceFormatted($arItem['PRICE']);
                // дополняем массив изображений
                if (!empty($arItem['PREVIEW_PICTURE'])) {
                    $arImagesId[] = $arItem['PREVIEW_PICTURE'];
                }
                $key = $arItem['IBLOCK_SECTION_ID'] != IBLOCK_SECTION_ID_SAUCES ? 'RECOMMENDED' : 'COMBINED_WITH';
                $this->arResult[$key][$arItem['ID']] = $arItem;
            }
            // "заполняем" товары фотками
            $arFiles = [];
            if (!empty($arImagesId)) {
                $rsImages = \Bitrix\Main\FileTable::getList(['filter' => ['=ID' => $arImagesId]]);
                while ($arFile = $rsImages->fetch()) {
                    $arFiles[$arFile['ID']] = CFile::GetFileSRC($arFile);
                }
            }
            foreach ($arKeys as $key) {
                foreach ($this->arResult[$key] as $id => $arElement) {
                    $this->arResult[$key][$id]['IMAGE'] =
                        (!empty($arElement['PREVIEW_PICTURE']) && !empty($arFiles[$arElement['PREVIEW_PICTURE']]))
                            ? $arFiles[$arElement['PREVIEW_PICTURE']] : null;
                }
            }
        }
        $this->includeComponentTemplate();
    }
}
