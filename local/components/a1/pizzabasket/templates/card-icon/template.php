<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use Bitrix\Main\Localization\Loc;
?>
<a href="<?= $arParams['ORDER_PAGE'] ?>" class="header__cart">
    <div class="header__cartIcon js-cartIcon">
        <span class="header__cartCounter js-cartCounter<?= empty($arResult['COUNT']) ? ' js-hidden' : '' ?>">
            <?= $arResult['COUNT'] ?>
        </span>
    </div>
    <div>
        <p class="header__label"><?= Loc::getMessage('BASKET') ?></p>
        <p class="header__text">
            <span class="js-totalPrice"><?= $arResult['PRICE_FORMATTED'] ?></span> ₽
        </p>
    </div>
</a>
