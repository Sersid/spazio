<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use Bitrix\Main\Localization\Loc;
?>
<a href="<?= $arParams['ORDER_PAGE'] ?>" class="header__cart">
    <div class="header__cartIcon js-cartIcon">
        <span class="header__cartCounter js-cartCounter<?= empty($arResult['COUNT']) ? ' js-hidden' : '' ?>">
            <?= $arResult['COUNT'] ?>
        </span>
    </div>
    <div>
        <p class="header__text"><span class="js-totalPrice"><?= $arResult['PRICE_FORMATTED'] ?></span> ₽</p>
    </div>
    <div class="cartDropdown">
        <ul class="cartDropdown__list js-cartList">
            <? foreach ($arResult['ITEMS'] as $arItem) { ?>
                <li class="cartDropdown__item js-catalogItem" data-product-id="<?= $arItem['PRODUCT_ID'] ?>">
                    <p class="cartDropdown__name"><?= $arItem['NAME'] ?></p>
                    <? if (!empty($arItem['PIZZA_SIZE'])) { ?>
                        <p class="cartDropdown__chars"><?= $arItem['PIZZA_SIZE'] ?></p>
                    <? } ?>
                    <? if (!empty($arItem['VOLUME'])) { ?>
                        <p class="cartDropdown__chars"><?= $arItem['VOLUME'] ?></p>
                    <? } ?>
                    <div class="counter counter_small js-counterBox">
                        <button aria-label="<?= Loc::getMessage(
                            'COUNTER_LESS'
                        ) ?>" class="counter__button counter__button_decr js-counter" data-action="LESS"></button>
                        <input type="text" value="<?= $arItem['QUANTITY'] ?>" readonly="readonly" class="counter__value js-quantity"/>
                        <button aria-label="<?= Loc::getMessage(
                            'COUNTER_MORE'
                        ) ?>" class="counter__button counter__button_incr js-counter" data-action="MORE"></button>
                    </div>
                    <p class="cartDropdown__price">
                        <?= $arItem['PRICE_FORMATTED'] ?>
                        <span class="currency">₽</span>
                    </p>
                    <button aria-label="<?= Loc::getMessage('DELETE') ?>" class="cartDropdown__remove js-remove"></button>
                </li>
            <? } ?>
        </ul>
        <div class="cartDropdown__total js-cartTotal">
            <p class="cartDropdown__text"><?= Loc::getMessage('ORDER_PRICE') ?>:</p>
            <p class="cartDropdown__price">
                <span class="js-totalPrice"><?= $arResult['PRICE_FORMATTED'] ?></span>
                <span class="currency">₽</span>
            </p>
        </div>
    </div>
</a>
