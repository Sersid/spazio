$(function() {
    // добавление к корзину
    $(document).on('click', '.js-addToCart', function() {
        $(this).attr('disabled', 'disabled');
        var catalogItem = $(this).closest('.js-catalogItem'),
            productId = catalogItem.data('product-id');
        $.ajax({
            url: '/ajax/basket/add.php',
            data: {productId: productId},
            dataType: 'json',
            success: function(basket) {
                catalogItem.remove();
                renderBasket(basket);
            }
        });
    });

    // уменьшить/увеличить количество
    var timerId;
    $(document).on('click', '.js-counter', function() {
        var catalogItem = $(this).closest('.js-catalogItem'),
            productId = catalogItem.data('product-id'),
            counterBox = catalogItem.find('.js-counterBox'),
            quantityInput = counterBox.find('.js-quantity'),
            quantity = parseFloat(quantityInput.val());
        switch ($(this).data('action')) {
            case 'LESS':
                quantity -= 1;
                break;
            case 'MORE':
                quantity += 1;
                break;
        }
        if (quantity <= 0) {
            catalogItem.remove();
        } else {
            quantityInput.val(quantity);
        }
        clearTimeout(timerId);
        timerId = setTimeout(function() {
            $.ajax({
                url: '/ajax/basket/set-quantity.php',
                data: {productId: productId, quantity: quantity},
                dataType: 'json',
                success: function(basket) {
                    renderBasket(basket);
                }
            });
        }, 500);
    });

    // удаление из корзины
    $(document).on('click', '.js-remove', function() {
        var item = $(this).closest('.js-catalogItem'),
            productId = item.data('product-id');

        item.remove();
        $.ajax({
            url: '/ajax/basket/remove.php',
            data: {productId: productId},
            dataType: 'json',
            success: function(basket) {
                renderBasket(basket);
            }
        });
    });

    // ренден корзины
    function renderBasket(basket) {
        $('.js-totalPrice').html(basket.PRICE_FORMATTED);
        console.log(basket.PRICE);
        if (basket.PRICE < 600) {
            $('.js-minPrice').removeClass('js-hidden');
        } else {
            $('.js-minPrice').addClass('js-hidden');
        }
        var cartList = '';
        $.each(basket.ITEMS, function(i, item) {
            cartList += '<li class="orderItems__item js-catalogItem" data-product-id="' + item.PRODUCT_ID + '">\n'
                + '<article class="orderItem">\n'
                + '<div class="orderItem__info">\n'
                + '<h2 class="orderItem__name">' + item.NAME + '</h2>\n';
            if (item.PIZZA_SIZE) {
                cartList += '<p class="orderItem__descr">' + item.PIZZA_SIZE + '</p>';
            }
            if (item.VOLUME) {
                cartList += '<p class="orderItem__descr">' + item.VOLUME + '</p>';
            }
            cartList += '</div>'
                + '<div class="counter js-counterBox">\n'
                + '<button aria-label="уменьшить количество товара" class="counter__button counter__button_decr js-counter" data-action="LESS"></button>\n'
                + '<input type="text" value="' + item.QUANTITY + '" readonly="readonly" class="counter__value js-quantity"/>\n'
                + '<button aria-label="увеличить количество товара в корзине" class="counter__button counter__button_incr js-counter" data-action="MORE"></button>\n'
                + '</div>'
                + '<p class="orderItem__price">'
                + '<span class="js-catalogItemPrice">' + item.PRICE_FORMATTED + '</span>'
                + '<span class="currency">₽</span>'
                + '</p>\n'
                + '<button aria-label="удалить элемент из корзины" class="orderItem__remove js-remove"></button>\n'
                + '</article>\n'
                + '</li>';
        });
        $('.js-cartList').html(cartList);


    }
});
