<?php
$MESS['COUNTER_LESS'] = 'уменьшить количество товара';
$MESS['COUNTER_MORE'] = 'увеличить количество товара в корзине';
$MESS['DELETE'] = 'удалить из корзины';
$MESS['RECOMMEND'] = 'Рекомендуем';
$MESS['ADD_TO_BASKET'] = 'В корзину';
$MESS['COMBINED_WITH'] = 'Отлично сочетается с:';
$MESS['PROMO_CODE'] = 'Промокод';
$MESS['APPLY'] = 'Применить';
$MESS['RETURN_TO_MENU'] = 'Вернуться в меню';
$MESS['ORDER_PRICE'] = 'Сумма заказа:';
$MESS['CHECKOUT'] = 'Оформить заказ';

