<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use Bitrix\Main\Localization\Loc;
?>
<? if (!empty($arResult['ITEMS'])) { ?>
    <ul class="orderItems js-cartList">
    <? foreach ($arResult['ITEMS'] as $arItem) { ?>
        <li class="orderItems__item js-catalogItem" data-product-id="<?= $arItem['PRODUCT_ID'] ?>">
            <article class="orderItem">
                <div class="orderItem__info">
                    <h2 class="orderItem__name"><?= $arItem['NAME'] ?></h2>
                    <? if (!empty($arItem['PIZZA_SIZE'])) { ?>
                        <p class="orderItem__descr"><?= $arItem['PIZZA_SIZE'] ?></p>
                    <? } ?>
                    <? if (!empty($arItem['VOLUME'])) { ?>
                        <p class="orderItem__descr"><?= $arItem['VOLUME'] ?></p>
                    <? } ?>
                </div>
                <div class="counter js-counterBox">
                    <button aria-label="<?= Loc::getMessage('COUNTER_LESS') ?>" class="counter__button counter__button_decr js-counter" data-action="LESS"></button>
                    <input type="text" value="<?= $arItem['QUANTITY'] ?>" readonly="readonly" class="counter__value js-quantity"/>
                    <button aria-label="<?= Loc::getMessage('COUNTER_MORE') ?>" class="counter__button counter__button_incr js-counter" data-action="MORE"></button>
                </div>
                <p class="orderItem__price">
                    <span class="js-catalogItemPrice"><?= $arItem['PRICE_FORMATTED'] ?></span>
                    <span class="currency">₽</span>
                </p>
                <button aria-label="<?= Loc::getMessage('DELETE') ?>" class="orderItem__remove js-remove"></button>
            </article>
        </li>
    <? } ?>
    </ul>
    <? if (!empty($arResult['RECOMMENDED'])) { ?>
        <section class="recomendation">
            <h2 class="heading heading_subheading"><?= Loc::getMessage('RECOMMEND') ?></h2>
            <div class="recomendation__layout">
                <div class="recomendation__carousel">
                    <? foreach ($arResult['RECOMMENDED'] as $arItem) { ?>
                        <div class="recomendation__slide js-catalogItem" data-product-id="<?= $arItem['ID'] ?>">
                            <article class="orderTile">
                                <img src="<?= $arItem['IMAGE'] ?>" alt="<?= $arItem['NAME']?>" class="orderTile__picture" title="<?= $arItem['NAME']?>"/>
                                <div class="orderTile__body">
                                    <h3 class="orderTile__name"><?= $arItem['NAME']?></h3>
                                    <p class="orderTile__price"><?= $arItem['PRICE']?> <span class="currency">₽</span></p>
                                    <button class="button button_long js-addToCart"><?= Loc::getMessage('ADD_TO_BASKET') ?></button>
                                </div>
                            </article>
                        </div>
                    <? } ?>
                </div>
            </div>
        </section>
    <? } ?>
    <? if (!empty($arResult['COMBINED_WITH'])) { ?>
        <section class="combination">
            <h2 class="heading heading_subheading"><?= Loc::getMessage('COMBINED_WITH') ?></h2>
            <ul class="combination__list">
                <? foreach ($arResult['COMBINED_WITH'] as $arItem) { ?>
                    <li class="combination__item js-catalogItem" data-product-id="<?= $arItem['ID'] ?>">
                        <article class="orderTile">
                            <img src="<?= $arItem['IMAGE'] ?>" alt="<?= $arItem['NAME']?>" class="orderTile__picture" title="<?= $arItem['NAME']?>"/>
                            <div class="orderTile__body">
                                <h3 class="orderTile__name"><?= $arItem['NAME']?></h3>
                                <p class="orderTile__price"><?= $arItem['PRICE']?> <span class="currency">₽</span></p>
                                <button class="button button_long js-addToCart"><?= Loc::getMessage('ADD_TO_BASKET') ?></button>
                            </div>
                        </article>
                    </li>
                <? } ?>
            </ul>
        </section>
    <? } ?>
    <div class="promoCode">
        <div class="promoCode__form">
            <? /*<input placeholder="<?= Loc::getMessage('PROMO_CODE') ?>" class="promoCode__input" type="text" />
            <button disabled="disabled" class="promoCode__button"><?= Loc::getMessage('APPLY') ?></button>*/ ?>
        </div>
    </div>
    <div class="orderTotal">
        <div class="orderTotal__sum">
            <p class="orderTotal__label"><?= Loc::getMessage('ORDER_PRICE') ?></p>
            <p class="orderTotal__price">
                <span class="js-totalPrice"><?= $arResult['PRICE_FORMATTED'] ?></span>
                <span class="currency">₽</span>
            </p>
            <p class="orderTotal__error js-minPrice <?= $arResult['PRICE'] > 600 ? 'js-hidden' : '' ?>">Минимальная сумма заказа 600 рублей</p>
        </div>
        <div class="orderTotal__nav">
            <a href="/" class="orderTotal__link orderTotal__link_back"><?= Loc::getMessage('RETURN_TO_MENU') ?></a>
            <a href="?step=2" class="orderTotal__link orderTotal__link_forward js-buttonNext"><?= Loc::getMessage('CHECKOUT') ?></a>
        </div>
    </div>
<? } else { ?>
    <p>Корзина пуста</p>
<? } ?>
