<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use Bitrix\Main\Localization\Loc;

Loc::loadLanguageFile(__FILE__);

$arComponentParameters = [
    'PARAMETERS' => [
        'ORDER_PAGE' => [
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("ORDER_PAGE"),
            "TYPE" => "STRING",
            "DEFAULT" => "/order/",
        ],
        'SHOW_RECOMMENDED' => [
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("SHOW_RECOMMENDED"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ],
        'SHOW_COMBINED_WITH' => [
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("SHOW_COMBINED_WITH"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ],
    ],
];
