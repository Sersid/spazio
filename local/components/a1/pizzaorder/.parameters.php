<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use Bitrix\Main\Localization\Loc;

Loc::loadLanguageFile(__FILE__);

$arComponentParameters = [
    'PARAMETERS' => [
        'BASKET_PAGE' => [
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("ORDER_PAGE"),
            "TYPE" => "STRING",
            "DEFAULT" => "/order/",
        ],
    ],
];
