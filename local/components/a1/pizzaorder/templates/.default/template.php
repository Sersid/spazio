<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<form class="js-orderForm" method="post">
    <? if (!empty($arResult['ERRORS'])) { ?>
        <div class="errors"><?= implode('<br />', $arResult['ERRORS']) ?></div>
    <? } ?>
    <input type="hidden" name="CREATE" value="Y">
    <input type="hidden" name="order">
    <fieldset class="orderDelivery">
        <legend class="orderDelivery__heading">Выберите способ доставки</legend>
        <div class="orderDelivery__layout">
            <input type="radio" value="3" id="delivery" name="DELIVERY" class="js-deliveryType orderDelivery__input visuallyHidden" checked /><label for="delivery" class="orderDelivery__label">Доставка</label>
            <input type="radio" value="2" id="self" name="DELIVERY" class="js-deliveryType orderDelivery__input visuallyHidden" /><label for="self" class="orderDelivery__label">Самовывоз</label>
        </div>
    </fieldset>
    <fieldset class="orderAddress js-delivery">
        <legend class="orderAddress__heading">Адрес доставки</legend>
        <div class="orderAddress__layout">
            <input type="text" placeholder="Улица" name="ADDRESS" value="<?= $arResult['ADDRESS'] ?>" class="orderAddress__input orderAddress__input_street" />
            <input type="text" placeholder="Дом / Корпус" name="HOUSE" value="<?= $arResult['HOUSE'] ?>" class="orderAddress__input orderAddress__input_house" />
            <input type="text" placeholder="Квартира / Офис" name="APARTMENT" value="<?= $arResult['APARTMENT'] ?>" class="orderAddress__input orderAddress__input_flat" />
            <input type="text" placeholder="Подъезд" name="ENTRANCE" value="<?= $arResult['ENTRANCE'] ?>" class="orderAddress__input orderAddress__input_entrance" />
            <input type="text" placeholder="Номер телефона" name="PHONE" value="<?= $arResult['PHONE'] ?>" class="orderAddress__input orderAddress__input_phone" />
            <input type="text" placeholder="Код двери" name="DOOR_CODE" value="<?= $arResult['DOOR_CODE'] ?>" class="orderAddress__input orderAddress__input_code" />
            <input type="text" placeholder="Этаж" name="FLOOR" value="<?= $arResult['FLOOR'] ?>" class="orderAddress__input orderAddress__input_floor" />
            <textarea name="COMMENT" class="orderAddress__input orderAddress__input_comment" rows="7" placeholder="Комментарий к адресу (пример: позвонить перед подъездом)"><?= $arResult['COMMENT'] ?></textarea>
            <div class="orderAddress__checkbox">
                <input type="checkbox" name="CONCIERGE" value="Y" <?= $arResult['CONCIERGE'] == 'Y' ? 'checked' : '' ?> id="concierge" class="orderAddress__checkbox visuallyHidden" />
                <label for="concierge" class="orderAddress__label"><span class="orderAddress__icon"></span> Консьерж</label>
            </div>
        </div>
    </fieldset>
    <fieldset class="orderPayment js-delivery">
        <legend class="orderPayment__heading">Способ оплаты:</legend>
        <div class="orderPayment__layout">
            <? foreach (['Наличными курьеру при получении', 'Картой курьеру при получении'] as $i => $payment) { ?>
                <input type="radio" id="payment-<?= $i ?>" name="PAYMENT" value="<?= $payment ?>" class="orderPayment__input visuallyHidden" <?= $arResult['PAYMENT'] == $payment ? 'checked' : '' ?> />
                <label for="payment-<?= $i ?>" class="orderPayment__label"><span class="orderPayment__icon"></span> <?= $payment ?></label>
            <? } ?>
        </div>
    </fieldset>
</form>
<div class="orderTotal">
    <div class="orderTotal__sum">
        <p class="orderTotal__label">Сумма заказа:</p>
        <p class="orderTotal__price"><?= priceFormatted($arResult['PRICE']) ?> <span class="currency">₽</span></p>
        <p class="orderTotal__error js-minPrice <?= $arResult['PRICE'] > 600 ? 'js-hidden' : '' ?>">Минимальная сумма заказа 600 рублей</p>
    </div>
    <div class="orderTotal__nav">
        <a href="/order/" class="orderTotal__link orderTotal__link_back">Назад в корзину</a>
        <a href="#" class="js-submitOrderForm orderTotal__link orderTotal__link_forward">Оформить заказ</a>
    </div>
</div>
