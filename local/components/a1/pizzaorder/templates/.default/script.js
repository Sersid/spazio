$(function() {
    $('.js-deliveryType').change(function() {
        if ($('.js-deliveryType:checked').attr('id') == 'delivery') {
            $('.js-delivery').show();
        } else {
            $('.js-delivery').hide();
        }
    }).trigger('change');
    $('.js-submitOrderForm').click(function() {
        $('.js-orderForm').trigger('submit');
        return false;
    });
});
