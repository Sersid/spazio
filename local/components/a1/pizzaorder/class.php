<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Context, Bitrix\Currency\CurrencyManager, Bitrix\Sale\Order, Bitrix\Sale\Basket, Bitrix\Sale\Delivery, Bitrix\Sale\PaySystem;

/**
 * Class A1PizzaOrderComponent
 * Класс для оформления заказа Spazio Pizza
 */
class A1PizzaOrderComponent extends \CBitrixComponent
{
    public function getDeliveries()
    {

    }

    /**
     * @inheritdoc
     */
    public function executeComponent()
    {
        Bitrix\Main\Loader::includeModule("sale");
        Bitrix\Main\Loader::includeModule("catalog");

        $siteId = Context::getCurrent()->getSite();
        $basket = Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(), $siteId);
        $this->arResult['PRICE'] = $basket->getPrice();

        if ($this->request->get('CREATE')) {
            \Bitrix\Sale\DiscountCouponsManager::init();
            $arProps = [
                'ADDRESS',
                'HOUSE',
                'APARTMENT',
                'ENTRANCE',
                'DOOR_CODE',
                'FLOOR',
                'CONCIERGE',
                'PHONE',
                'COMMENT',
                'DELIVERY',
                'PAYMENT',
            ];
            foreach ($arProps as $prop) {
                $this->arResult[$prop] = htmlspecialcharsbx((string)$this->request->get($prop));
            }
            $this->arResult['CONCIERGE'] = (string)$this->request->get('CONCIERGE') == 'Y' ? 'Y' : 'N';
            if ($this->arResult['DELIVERY'] == 3 && empty($this->arResult['PHONE'])) {
                $this->arResult['ERRORS'][] = 'Поле "Телефон" обязательное для заполнения';
            }
            if (!in_array($this->arResult['DELIVERY'], [2,3])) {
                $this->arResult['ERRORS'][] = 'Неверное поле "Доставка"';
            }
            if ($this->arResult['PRICE'] < 600) {
                $this->arResult['ERRORS'][] = 'Минимальная сумма заказа 600 рублей';
            }
            if ($this->arResult['DELIVERY'] == 3 && empty($this->arResult['PAYMENT'])) {
                $this->arResult['ERRORS'][] = 'Выберите "Способ оплаты"';
            }

            if (empty($this->arResult['ERRORS'])) {
                $currencyCode = CurrencyManager::getBaseCurrency();

                $order = Order::create($siteId, 1);
                $order->setPersonTypeId(1);
                $order->setField('CURRENCY', $currencyCode);
                $order->setField('COMMENTS', $this->arResult['COMMENT']);
                $order->setBasket($basket);


                /*Shipment*/
                $shipmentCollection = $order->getShipmentCollection();
                $shipment = $shipmentCollection->createItem();
                $service = Delivery\Services\Manager::getById($this->arResult['DELIVERY']);
                $shipment->setFields(
                    array(
                        'DELIVERY_ID' => $service['ID'],
                        'DELIVERY_NAME' => $service['NAME'],
                    )
                );

                $order->doFinalAction(true);
                // Свойcтва
                $propertyCollection = $order->getPropertyCollection();
                foreach ($propertyCollection->getGroups() as $group) {
                    foreach ($propertyCollection->getGroupProperties($group['ID']) as $property) {
                        $p = $property->getProperty();
                        if (in_array($p['CODE'], $arProps)) {
                            $property->setValue($this->arResult[$p['CODE']]);
                        }
                    }
                }

                // Сохраняем
                $result = $order->save();
                LocalRedirect('/order/?step=3&ID=' . $order->getId());
            }
        }
        $this->includeComponentTemplate();
    }
}
