<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>
<? if ($_GET['step'] == 3) { ?>
    <? $APPLICATION->SetTitle("Заказ #" . (int)$_GET['ID'] . " принят"); ?>
    <section class="thanks">
        <p class="thanks__text">Спасибо, что вы с нами. Наш менеждер свяжется с вами в ближайшее время<br> для уточнения
            деталей заказа.</p>
        <p class="thanks__text">Если мы не связались с вами в течении 10 минут, позвоните по номеру,<br> указанному ниже
        </p>
        <p class="thanks__text thanks__text_phone">+7 3462 38-00-06</p>
    </section>
    <div class="orderTotal">
        <div class="orderTotal__nav"><a href="/" class="orderTotal__link orderTotal__link_forward">На главную</a></div>
    </div>
<? } elseif ($_GET['step'] == 2) { ?>
    <? $APPLICATION->SetTitle("Оформить заказ"); ?>
    <? $APPLICATION->IncludeComponent(
        "a1:pizzaorder",
        ".default",
        array(),
        false
    ); ?>
<? } else { ?>
    <? $APPLICATION->SetTitle("Корзина"); ?>
    <? $APPLICATION->IncludeComponent(
        "a1:pizzabasket",
        ".default",
        array(
            "COMPONENT_TEMPLATE" => ".default",
            "ORDER_PAGE" => "/order/",
            "SHOW_RECOMMENDED" => "Y",
            "SHOW_COMBINED_WITH" => "Y",
        ),
        false
    ); ?>
<? } ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
