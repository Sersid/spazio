<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("404 error");
?>
<div class="error404">
    <img src="/local/templates/spazio/assets/images/404.png" alt="запрашиваемой страницы не существует" class="error404__image">
    <!-- /.error404__image -->
    <div class="error404__message">
        <p class="error404__text">Кажется, вы прилетели не туда.</p>
        <!-- /.error404__text -->
        <a href="/" class="error404__link">Вернуться на главную</a>
        <!-- /.error404__link -->
    </div>
    <!-- /.error404__message -->
</div>
<!-- /.error404 -->
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
