<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Пицца SPAZIO");
?><?$APPLICATION->IncludeComponent(
	"a1:pizzacatalog",
	".default",
	Array(
		"CATALOG_PRICE_TYPE" => "1",
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "catalog"
	)
);?> <section class="info">
<h2 class="sectionHeading info__heading">Доставка и оплата</h2>
<div class="info__col">
 <article class="delivery">
	<h3 class="delivery__heading">Условия доставки:</h3>
	<p>
		 Доставляем в течении 1 часа.
	</p>
	<p>
		 Время доставки может быть увеличено при неблагоприятных условиях погоды, дорожной обстановки и удаленности района.&nbsp;
	</p>
	<p>
		 Доставку заказа на определенное время осуществляем с интервалом + - 10 минут.&nbsp;
	</p>
	<p class="delivery__text">
		 Доставка по городу бесплатно при минимальном заказе 600 руб.
	</p>
	<p class="delivery__text">
 <b>Платная доставка по удалённым районам:</b>&nbsp;<br>
	</p>
	<ul class="delivery__list">
		<li class="delivery__item">Район ЖД вокзал, за ЖД, Промзона&nbsp;&nbsp;- 100 рублей</li>
		<li class="delivery__item">п.Белый Яр, п.Снежный, Кедр, п.Юность, п.Лунный, п.Кедровый, п.Финский&nbsp;- 150 рублей</li>
		<li class="delivery__item">п. Барсово, п.Солнечный, п.Таёжный, п.Дорожный, Аэропорт.&nbsp;- 200 рублей</li>
	</ul>
	<p>
 <br>
	</p>
 </article>
</div>
<div class="info__col">
 <article class="payment">
	<h3 class="payment__heading">Как оплатить заказ:</h3>
	 Минимальная сумма заказа по городу 600 рублей.<br>
	 Минимальная сумма заказа в удаленные районы 1200 рублей.
	<p>
	</p>
	<p>
		 При низкой температуре окружающей среды ниже 30 градусов + 50 рублей.&nbsp;
	</p>
	<p>
		 По всем вопросам доставки, Вам ответит наш доброжелательный оператор.&nbsp;
	</p>
	<p>
		 При самовывозе скидка 15%. Самовывоз осуществляется по адресу: ул.Республики 76/1
	</p>
	<p>
		<br>
	</p>
	<ol class="payment__list">
		<li class="payment__item">
		<h4 class="payment__listHeading">Наличный расчет</h4>
		<p class="payment__text">
			 Если товар доставляется курьером, то оплата осуществляется наличными курьеру в руки.
		</p>
 </li>
		<li class="payment__item">
		<h4 class="payment__listHeading">Оплата картой курьеру</h4>
		<p class="payment__text">
			 Если товар доставляется курьером, то оплата осуществляется картой через терминал.
		</p>
 </li>
	</ol>
 </article>
</div>
<div class="info__col">
 <article class="map">
	<h3 class="map__heading">Зона доставки:</h3>
<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A8c05d10fd2d598766f2a9683cc8ba2036b5ccb78a0bd5aa685c3bcc6f9e477f6&amp;width=350&amp;height=291&amp;lang=ru_RU&amp;scroll=true"></script>
<?php/*
 <img alt="карта доставки" src="<?= $assetsPath ?>/images/map.jpg" class="map__image" title="">
*/?>
 </article>
</div>
 </section> <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>